#!/usr/bin/env bash

gst-package -t ~/.st package.xml 1> /dev/null
gst --no-gc-message script/run.st
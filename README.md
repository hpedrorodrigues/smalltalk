# Smalltalk 80 - A simple CRUD

A simple Smalltalk CRUD on virtual memory :)

A school work

To run this project just install **GNU Smalltalk** (`brew install gnu-smalltalk`) and type `./script/run.sh`.

![Smalltalk](./assets/Smalltalk.png)

## References

- [AngelFire](http://www.angelfire.com/tx4/cus/notes/smalltalk.html)
- [Comparison of OrderedCollections in Smalltalk, Java, Objective-C, Ruby, Python, and C#](http://www.luisdelarosa.com/2005/05/24/comparison-of-orderedcollections-in-smalltalk-java-objective-c-ruby-python-and-c/)
- [List comprehension using select and collect](http://smalltalk.gnu.org/blog/sblinn/list-comprehension-using-select-and-collect)
- [Smalltalk Case Statement](http://c2.com/cgi/wiki?SmalltalkCaseStatement)
- [GNU Smalltalk](http://smalltalk.gnu.org/)
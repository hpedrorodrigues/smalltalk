Object subclass: Runner [

    | option switch students teachers disciplines classes
      currentStudent currentTeacher currentDiscipline currentClazz
      filteredStudents studentId studentName studentAge
      filteredTeachers teacherId teacherName teacherSchoolName
      filteredDisciplines disciplineId disciplineTitle
      filteredClasses classId classTeacher classDiscipline classStudents classCurrentStudents filteredClassCurrentStudents |

    insertStudent [
        Logger log: 'Digite o `id` do estudante'.
        studentId := stdin nextLine asInteger.

        filteredStudents := students select: [ :student | student id == studentId ].

        (filteredStudents isEmpty)
            ifTrue: [
                Logger log: 'Informe o `nome` do estudante'.
                studentName := stdin nextLine.

                Logger log: 'Informe a `idade` do estudante'.
                studentAge := stdin nextLine asInteger.

                currentStudent := Student new.
                currentStudent id: studentId.
                currentStudent name: studentName.
                currentStudent age: studentAge.

                students add: currentStudent.

                Logger logAndBreak: 'Estudante inserido com sucesso! :)'.
            ]
            ifFalse: [ Logger log: 'Já existe um estudante com o id informado! :(' ].

        Logger breakLine.
    ]

    editStudent [
        Logger log: 'Digite o `id` do estudante'.
        studentId := stdin nextLine asInteger.

        filteredStudents := students select: [ :student | student id == studentId ].

        (filteredStudents isEmpty)
            ifTrue: [ Logger logAndBreak: 'O aluno com o `id` informado não foi encontrado! :(' ]
            ifFalse: [
                Logger log: 'Informe o `nome` do estudante'.
                studentName := stdin nextLine.

                Logger log: 'Informe a `idade` do estudante'.
                studentAge := stdin nextLine asInteger.

                currentStudent := filteredStudents first.
                currentStudent id: studentId.
                currentStudent name: studentName.
                currentStudent age: studentAge.

                Logger logAndBreak: 'Estudante alterado com sucesso! :)'.
            ]
    ]

    removeStudent [
        Logger log: 'Digite o `id` do estudante'.
        studentId := stdin nextLine asInteger.

        filteredStudents := students select: [ :student | student id == studentId ].

        (filteredStudents isEmpty)
            ifTrue: [ Logger logAndBreak: 'O aluno com o `id` informado não foi encontrado! :(' ]
            ifFalse: [
                filteredClasses := OrderedCollection new.

                classes do: [ :clazz |
                    classCurrentStudents := clazz students select: [ :student | student id == studentId ].

                    (classCurrentStudents isEmpty)
                        ifFalse: [ filteredClasses add: clazz ]
                ].

                (filteredClasses isEmpty)
                    ifTrue: [
                        students remove: filteredStudents first.
                        Logger log: 'Aluno removido com sucesso! :)'.
                    ]
                    ifFalse: [
                        Logger log: ('O estudante com o `id` informado não pode ser removido porque faz parte da classe com `id` = `', filteredClasses first id asString, '` :(').
                        Logger log: 'Por favor, remova primeiro a classe :)'.
                    ].
            ].

        Logger breakLine.
    ]

    listStudents [
        (students isEmpty)
            ifTrue: [ Logger logAndBreak: 'Não há estudantes cadastrados! :(' ]
            ifFalse: [
                Logger logAndBreak: 'Estudantes:'.
                students do: [ :student | Logger log: student asString ]
            ].

        Logger breakLine.
    ]

    insertTeacher [
        Logger log: 'Digite o `id` do professor'.
        teacherId := stdin nextLine asInteger.

        filteredTeachers := teachers select: [ :teacher | teacher id == teacherId ].

        (filteredTeachers isEmpty)
            ifTrue: [
                Logger log: 'Informe o `nome` do professor'.
                teacherName := stdin nextLine.

                Logger log: 'Informe o `nome da escola` do professor'.
                teacherSchoolName := stdin nextLine.

                currentTeacher := Teacher new.
                currentTeacher id: teacherId.
                currentTeacher name: teacherName.
                currentTeacher schoolName: teacherSchoolName.

                teachers add: currentTeacher.

                Logger logAndBreak: 'Professor inserido com sucesso! :)'.
            ]
            ifFalse: [ Logger log: 'Já existe um professor com o id informado! :(' ].

        Logger breakLine.
    ]

    editTeacher [
        Logger log: 'Digite o `id` do professor'.
        teacherId := stdin nextLine asInteger.

        filteredTeachers := teachers select: [ :teacher | teacher id == teacherId ].

        (filteredTeachers isEmpty)
            ifTrue: [ Logger logAndBreak: 'O professor com o `id` informado não foi encontrado! :(' ]
            ifFalse: [
                Logger log: 'Informe o `nome` do professor'.
                teacherName := stdin nextLine.

                Logger log: 'Informe o `nome da escola` do professor'.
                teacherSchoolName := stdin nextLine.

                currentTeacher := filteredTeachers first.
                currentTeacher id: teacherId.
                currentTeacher name: teacherName.
                currentTeacher schoolName: teacherSchoolName.

                Logger logAndBreak: 'Professor alterado com sucesso! :)'.
            ].

        Logger breakLine.
    ]

    removeTeacher [
        Logger log: 'Digite o `id` do professor'.
        teacherId := stdin nextLine asInteger.

        filteredTeachers := teachers select: [ :teacher | teacher id == teacherId ].

        (filteredTeachers isEmpty)
            ifTrue: [ Logger logAndBreak: 'O professor com o `id` informado não foi encontrado! :(' ]
            ifFalse: [
                filteredClasses := classes select: [ :clazz | clazz teacher id == teacherId ].

                (filteredClasses isEmpty)
                    ifTrue: [
                        teachers remove: filteredTeachers first.
                        Logger log: 'Professor removido com sucesso! :)'.
                    ]
                    ifFalse: [
                        Logger log: ('O professor com o `id` informado não pode ser removido porque faz parte da classe com `id` = `', filteredClasses first id asString, '` :(').
                        Logger log: 'Por favor, remova primeiro a classe :)'.
                    ].
            ].

        Logger breakLine.
    ]

    listTeachers [
        (teachers isEmpty)
            ifTrue: [ Logger logAndBreak: 'Não há professores cadastrados! :(' ]
            ifFalse: [
                Logger logAndBreak: 'Professores:'.
                teachers do: [ :teacher | Logger log: teacher asString ]
            ].

        Logger breakLine.
    ]

    insertDiscipline [
        Logger log: 'Digite o `id` da disciplina'.
        disciplineId := stdin nextLine asInteger.

        filteredDisciplines := disciplines select: [ :discipline | discipline id == disciplineId ].

        (filteredDisciplines isEmpty)
            ifTrue: [
                Logger log: 'Informe o `título` da disciplina'.
                disciplineTitle := stdin nextLine.

                currentDiscipline := Discipline new.
                currentDiscipline id: disciplineId.
                currentDiscipline title: disciplineTitle.

                disciplines add: currentDiscipline.

                Logger logAndBreak: 'Disciplina inserida com sucesso! :)'.
            ]
            ifFalse: [ Logger log: 'Já existe uma disciplina com o id informado! :(' ].

        Logger breakLine.
    ]

    editDiscipline [
        Logger log: 'Digite o `id` da disciplina'.
        disciplineId := stdin nextLine asInteger.

        filteredDisciplines := disciplines select: [ :discipline | discipline id == disciplineId ].

        (filteredDisciplines isEmpty)
            ifTrue: [ Logger logAndBreak: 'A disciplina com o `id` informado não foi encontrada! :(' ]
            ifFalse: [
                Logger log: 'Informe o `título` da disciplina'.
                disciplineTitle := stdin nextLine.

                currentDiscipline := filteredDisciplines first.
                currentDiscipline id: disciplineId.
                currentDiscipline title: disciplineTitle.

                Logger logAndBreak: 'Disciplina alterada com sucesso! :)'.
            ].

        Logger breakLine.
    ]

    removeDiscipline [
        Logger log: 'Digite o `id` da disciplina'.
        disciplineId := stdin nextLine asInteger.

        filteredDisciplines := disciplines select: [ :discipline | discipline id == disciplineId ].

        (filteredDisciplines isEmpty)
            ifTrue: [ Logger logAndBreak: 'A disciplina com o `id` informado não foi encontrada! :(' ]
            ifFalse: [
                filteredClasses := classes select: [ :clazz | clazz discipline id == disciplineId ].

                (filteredClasses isEmpty)
                    ifTrue: [
                        disciplines remove: filteredDisciplines first.
                        Logger log: 'Disciplina removida com sucesso! :)'.
                    ]
                    ifFalse: [
                        Logger log: ('A disciplina com o `id` informado não pode ser removida porque faz parte da classe com `id` = `', filteredClasses first id asString, '` :(').
                        Logger log: 'Por favor, remova primeiro a classe :)'.
                    ].
            ].

        Logger breakLine.
    ]

    listDisciplines [
        (disciplines isEmpty)
            ifTrue: [ Logger logAndBreak: 'Não há disciplinas cadastradas! :(' ]
            ifFalse: [
                Logger logAndBreak: 'Disciplinas:'.
                disciplines do: [ :discipline | Logger log: discipline asString ]
            ].

        Logger breakLine.
    ]

    insertClass [
        Logger log: 'Digite o `id` da classe'.
        classId := stdin nextLine asInteger.

        filteredClasses := classes select: [ :clazz | clazz id == classId ].

        (filteredClasses isEmpty)
            ifTrue: [
                Logger log: 'Digite o `id` do professor'.
                teacherId := stdin nextLine asInteger.

                filteredTeachers := teachers select: [ :teacher | teacher id == teacherId ].

                (filteredTeachers isEmpty)
                    ifTrue: [ Logger logAndBreak: 'O professor com o `id` informado não foi encontrado :(' ]
                    ifFalse: [
                        Logger log: 'Digite o `id` da disciplina'.
                        disciplineId := stdin nextLine asInteger.

                        filteredDisciplines := disciplines select: [ :discipline | discipline id == disciplineId ].

                        (filteredDisciplines isEmpty)
                            ifTrue: [ Logger logAndBreak: 'A disciplina com o `id` informado não foi encontrada :(' ]
                            ifFalse: [
                                Logger log: 'Digite o `id` do estudante ou `0` para sair'.
                                studentId := stdin nextLine asInteger.

                                classCurrentStudents := OrderedCollection new.

                                [studentId ~= 0]
                                    whileTrue: [
                                        filteredClassCurrentStudents := classCurrentStudents select: [ :student | student id == studentId ].
                                        filteredStudents := students select: [ :student | student id == studentId ].

                                        (filteredStudents isEmpty)
                                            ifTrue: [ Logger logAndBreak: 'O estudante com o `id` informado não foi encontrado :(' ]
                                            ifFalse: [
                                                (filteredClassCurrentStudents isEmpty)
                                                    ifTrue: [
                                                        currentStudent := filteredStudents first.
                                                        classCurrentStudents add: currentStudent.

                                                        Logger logAndBreak: 'O estudante foi adicionado a essa turma com sucesso! :)'.
                                                    ]
                                                    ifFalse: [ Logger logAndBreak: 'O estudante com o id informado já foi cadastrado :(' ].
                                            ].
                                        Logger logAndBreak: 'Digite o `id` do estudante ou `0` para sair'.
                                        studentId := stdin nextLine asInteger.
                                    ].

                                    currentTeacher := filteredTeachers first.
                                    currentDiscipline := filteredDisciplines first.

                                    currentClazz := Clazz new.
                                    currentClazz id: classId.
                                    currentClazz teacher: currentTeacher.
                                    currentClazz discipline: currentDiscipline.

                                    (classCurrentStudents isEmpty)
                                        ifFalse: [ classCurrentStudents do: [ :student | currentClazz students add: student ] ].

                                    classes add: currentClazz.

                                    Logger logAndBreak: 'Turma inserida com sucesso! :)'.
                            ].
                    ].
            ]
            ifFalse: [ Logger log: 'Já existe uma classe com o id informado! :(' ].

        Logger breakLine.
    ]

    editClass [

    ]

    removeClass [
        Logger log: 'Digite o `id` da classe'.
        classId := stdin nextLine asInteger.

        filteredClasses := classes select: [ :clazz | clazz id == classId ].

        (filteredClasses isEmpty)
            ifTrue: [ Logger logAndBreak: 'A turma com o `id` informado não foi encontrada! :(' ]
            ifFalse: [
                classes remove: filteredClasses first.
                Logger log: 'Turma removida com sucesso! :)'.
            ].

        Logger breakLine.
    ]

    listClasses [
        (classes isEmpty)
            ifTrue: [ Logger logAndBreak: 'Não há turmas cadastradas! :(' ]
            ifFalse: [
                Logger logAndBreak: 'Classes:'.
                classes do: [ :clazz | clazz info ]
            ].

        Logger breakLine.
    ]

    listAllData [
        self listStudents.
        self listTeachers.
        self listDisciplines.
        self listClasses.
        Logger breakLine.
    ]

    run [
        students := OrderedCollection new.
        teachers := OrderedCollection new.
        disciplines := OrderedCollection new.
        classes := OrderedCollection new.

        Util displayHeaderOptions.
        option := stdin nextLine asInteger.

        [option ~= Constant exit]
            whileTrue: [

                switch := Switch new.
                switch init: option.

                switch
                    case: Constant insertStudent; do: [ self insertStudent ];
                    case: Constant editStudent; do: [ self editStudent ];
                    case: Constant removeStudent; do: [ self removeStudent ];
                    case: Constant listStudents; do: [ self listStudents ];
                    case: Constant insertTeacher; do: [ self insertTeacher ];
                    case: Constant editTeacher; do: [ self editTeacher ];
                    case: Constant removeTeacher; do: [ self removeTeacher ];
                    case: Constant listTeachers; do: [ self listTeachers ];
                    case: Constant insertDiscipline; do: [ self insertDiscipline ];
                    case: Constant editDiscipline; do: [ self editDiscipline ];
                    case: Constant removeDiscipline; do: [ self removeDiscipline ];
                    case: Constant listDisciplines; do: [ self listDisciplines ];
                    case: Constant insertClass; do: [ self insertClass ];
                    case: Constant editClass; do: [ self editClass ];
                    case: Constant removeClass; do: [ self removeClass ];
                    case: Constant listClasses; do: [ self listClasses ];
                    case: Constant listAllData; do: [ self listAllData ];
                    default: [ Logger logAndBreak: 'Opção selecionada inválida!' ].

                Util displayHeaderOptions.
                option := stdin nextLine asInteger.
            ].

        Util displayFooterOptions.
    ]
]
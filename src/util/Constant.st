Object subclass: Constant [

    Constant class [

        exit [ ^ 1 ]

        insertStudent [ ^ 2 ]

        editStudent [ ^ 3 ]

        removeStudent [ ^ 4 ]

        listStudents [ ^ 5 ]

        insertTeacher [ ^ 6 ]

        editTeacher [ ^ 7 ]

        removeTeacher [ ^ 8 ]

        listTeachers [ ^ 9 ]

        insertDiscipline [ ^ 10 ]

        editDiscipline [ ^ 11 ]

        removeDiscipline [ ^ 12 ]

        listDisciplines [ ^ 13 ]

        insertClass [ ^ 14 ]

        editClass [ ^ 15 ]

        removeClass [ ^ 16 ]

        listClasses [ ^ 17 ]

        listAllData [ ^ 18 ]
    ]
]
Object subclass: Util [

    Util class [

        displayHeaderOptions [

            |splittedMessage message |

            message := '---------------------------------------------------\'.
            message := (message, '| FA7: Sistemas de Informação                     |\').
            message := (message, '| Disciplina: Linguagens de Programação           |\').
            message := (message, '| Linguagem: Smalltalk                            |\').
            message := (message, '| Aluno: Pedro Rodrigues                          |\').
            message := (message, '|-------------------------------------------------|\').
            message := (message, '|                                                 |\').
            message := (message, '| Digite `', Constant exit asString, '` para sair                            |\').
            message := (message, '| Digite `', Constant insertStudent asString, '` para inserir um `estudante`          |\').
            message := (message, '| Digite `', Constant editStudent asString, '` para editar um `estudante`           |\').
            message := (message, '| Digite `', Constant removeStudent asString, '` para remover um `estudante`          |\').
            message := (message, '| Digite `', Constant listStudents asString, '` para listar todos os `estudantes`    |\').
            message := (message, '| Digite `', Constant insertTeacher asString, '` para inserir um `professor`          |\').
            message := (message, '| Digite `', Constant editTeacher asString, '` para editar um `professor`           |\').
            message := (message, '| Digite `', Constant removeTeacher asString, '` para remover um `professor`          |\').
            message := (message, '| Digite `', Constant listTeachers asString, '` para listar todos os `professores`   |\').
            message := (message, '| Digite `', Constant insertDiscipline asString, '` para inserir uma `disciplina`       |\').
            message := (message, '| Digite `', Constant editDiscipline asString, '` para editar uma `disciplina`        |\').
            message := (message, '| Digite `', Constant removeDiscipline asString, '` para remover uma `disciplina`       |\').
            message := (message, '| Digite `', Constant listDisciplines asString, '` para listar todas as `disciplinas`  |\').
            message := (message, '| Digite `', Constant insertClass asString, '` para inserir uma `turma`            |\').
            message := (message, '| Digite `', Constant editClass asString, '` para editar uma `turma`             |\').
            message := (message, '| Digite `', Constant removeClass asString, '` para remover uma `turma`            |\').
            message := (message, '| Digite `', Constant listClasses asString, '` para listar todas as `turmas`       |\').
            message := (message, '| Digite `', Constant listAllData asString, '` para listar todos os `dados`        |\').
            message := (message, '---------------------------------------------------\').
            message := (message, ' \').

            splittedMessage := message subStrings: $\.
            splittedMessage do: [ :subMessage | Logger log: subMessage ].
        ]

        messageFromOrderedCollection: list [
            | message |

            message := ''.

            (list isEmpty)
                ifTrue: [ Logger log: 'Lista vazia' ]
                ifFalse: [
                    list do: [ :elem |
                        (elem == list first)
                            ifTrue: [ message := (message, elem asString) ]
                            ifFalse: [ message := (message, ' - ', elem asString) ].
                    ]
                ].

            ^ message.
        ]

        displayFooterOptions [
            Logger breakLine.
            Logger log: '#valeu ;)'.
        ]
    ]
]
Object subclass: Switch [

    | runDefault value case |

    init: aValue [
        value := aValue.
        runDefault := true
    ]

    case: anotherValue [ case := [ value == anotherValue ] ]

    do: block [
        (case value)
            ifTrue: [
                runDefault := false.
                block value.
            ]
    ]

    default: block [
        (runDefault)
            ifTrue: [ block value ].
    ]
]
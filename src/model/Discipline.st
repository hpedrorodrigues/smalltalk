Entity subclass: Discipline [

    | title |

    title [ ^ title ]

    title: aTitle [ title := aTitle ]

    asString [ ^ super asString, ', ', 'Título: ',  title asString ]
]
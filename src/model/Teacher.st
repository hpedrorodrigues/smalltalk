Person subclass: Teacher [

    | schoolName |

    schoolName [ ^ schoolName ]

    schoolName: aSchoolName [ schoolName := aSchoolName ]

    asString [ ^ super asString, ', ', 'Nome da escola: ', schoolName asString ]
]
Object subclass: Entity [

    | id |

    id [ ^ id ]

    id: aId [ id := aId ]

    asString [ ^ 'Id: ', id asString ]
]
Entity subclass: Clazz [

    | teacher discipline students |

    teacher [ ^ teacher ]

    teacher: aTeacher [ teacher := aTeacher ]

    discipline [ ^ discipline ]

    discipline: aDiscipline [ discipline := aDiscipline ]

    students [
        (students isNil)
            ifTrue: [ students := OrderedCollection new. ].

        ^ students.
    ]

    info [
        Logger log: ('Id: ', super id asString).
        Logger log: ('Professor: ', teacher asString).
        Logger log: ('Disciplina: ', discipline asString).
        Logger log: ('Estudantes: ', (Util messageFromOrderedCollection: students) asString).
        Logger breakLine.
    ]
]
Entity subclass: Person [

    | name |

    name [ ^ name ]

    name: aName [ name := aName ]

    asString [ ^ super asString, ', ', 'Nome: ',  name asString ]
]
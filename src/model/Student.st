Person subclass: Student [

    | age |

    age [ ^ age ]

    age: aAge [ age := aAge ]

    asString [ ^ super asString, ', ', 'Idade: ', age asString ]
]
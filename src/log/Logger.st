Object subclass: Logger [

    Logger class [

        breakLine [ ' ' displayNl ]

        log: message [ message displayNl ]

        logAndBreak: message [
            self breakLine.
            self log: message.
            self breakLine.
        ]
    ]
]